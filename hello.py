import sys
from os import getenv
from time import sleep
from socket import gethostbyaddr
from requests import get, post
from multiprocess import Process
from flask import Flask, request, render_template


def resolve_external_url():
    ip = get('https://checkip.amazonaws.com').text.strip()
    host = gethostbyaddr(ip)[0]
    port = int(getenv('EXTERNAL_PORT', getenv('FLASK_RUN_PORT', '5000')))
    return f'http://{host}:{port}/hello'


external_url = getenv('EXTERNAL_URL', resolve_external_url())

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html', my_url=external_url)

@app.route('/hello', methods=['POST'])
def hello():
    message = request.json['hello']
    print()
    return render_template('hello.html', message=message)

def register(target_url):
    print("Registering at {target_url}...")
    response = post(url=target_url, json={'url': external_url}).text.strip()
    print(response)

def main():
    endpoint = getenv('ENDPOINT') or sys.exit('ENDPOINT is not defined')
    args = {'port': getenv('FLASK_RUN_PORT', '5000'), 'host': getenv('FLASK_RUN_HOST', '0.0.0.0')}
    ps = Process(target=app.run, kwargs=args)
    ps.start()
    sleep(1)
    print(f'API is reachable at: {external_url}')
    try:
        register(endpoint)
    except Exception as err:
        ps.terminate()
        sys.exit(f'ERROR: Failed registering at the backend: {err}')
    else:
        try:
            ps.join()
        except KeyboardInterrupt:
            ps.terminate()


if __name__ == '__main__':
    main()
